/*
 * Copyright 2020 Larry1123
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// types

import type {
  Hooks,
  CommandContext,
  Package,
  ConfigurationValueMap,
  ResolveOptions,
  Resolver,
  PackageExtensions,
} from '@yarnpkg/core'
import type {
  PortablePath,
} from '@yarnpkg/fslib'
import type {
  Usage, 
} from 'clipanion'

import type {} from '@yarnpkg/plugin-version'

// imports

import {
  Cache,
  Configuration,
  Project,
  StreamReport,
  MessageName,
  PackageExtensionStatus,
  PackageExtensionType,
} from '@yarnpkg/core'
import {
  MultiResolver,
} from '../MultiResolver'
import {
  LockfileResolver,
} from '@yarnpkg/core'
import {
  WorkspaceRequiredError, 
} from '@yarnpkg/cli'
import {
  ppath,
  xfs,
  npath,
  Filename,
} from '@yarnpkg/fslib'
import {
  patchUtils, 
} from '@yarnpkg/plugin-patch'
import {
  packUtils, 
} from '@yarnpkg/plugin-pack'
import {
  getPnpPath,
} from '@yarnpkg/plugin-pnp'
import {
  Command,
  Option,
} from 'clipanion'

import {
  dependenciesUtils, 
} from '@larry1123/yarn-utils'

import {
  copyFile,
  copyFolder, 
} from '../util'
import {
  ProductionInstallFetcher, 
} from '../ProductionInstallFetcher'
import {
  ProductionInstallResolver, 
} from '../ProductionInstallResolver'

type KeysMatching<T, V> = {
  [K in keyof T]-?: T[K] extends V ? K : never
}[keyof T]

export class ProdInstall extends Command<CommandContext> {
  static paths = [['prod-install']]

  outDirectory: string = Option.String({ name: 'outDirectory' })

  json: boolean = Option.Boolean(`--json`, false, {description: 'Format the output as an NDJSON stream'})

  stripTypes: boolean = Option.Boolean('--strip-types', true, {description: 'Use --no-strip-types to not strip `@types/*` dependencies'})

  pack: boolean = Option.Boolean('--pack', false)

  silent: boolean = Option.Boolean('--silent', false, { hidden: true })

  production: boolean = Option.Boolean(`--production`, true, {description: 'Use --no-production to not strip devDependencies'})

  injectCjsPnp: string = Option.String(`--injectCjsPnp`, '', {description: 'Expermental!: Use --injectCjsPnp to inject a require to the .pnp.cjs file, this requires pack to be enabled'})

  static usage: Usage = Command.Usage({
    description: 'INSTALL!',
    details: 'prod only install',
    examples: [
      [`Install the project with only prod dependencies`, `$0 prod-install`],
    ],
  })

  async execute(): Promise<0 | 1> {
    const configuration = await Configuration.find(
      this.context.cwd,
      this.context.plugins,
    )
    const {
      project, workspace, 
    } = await Project.find(
      configuration,
      this.context.cwd,
    )

    await project.restoreInstallState()

    if (!workspace) {
      throw new WorkspaceRequiredError(project.cwd, this.context.cwd)
    }
    const cache = await Cache.find(configuration, {
      immutable: true,
      check: false,
    })

    const rootDirectoryPath = project.topLevelWorkspace.cwd
    const outDirectoryPath: PortablePath = npath.isAbsolute(this.outDirectory)
      ? npath.toPortablePath(this.outDirectory)
      : ppath.join(workspace.cwd, npath.toPortablePath(this.outDirectory))

    const report = await StreamReport.start(
      {
        configuration,
        json: this.json,
        stdout: this.context.stdout,
      },
      async (report: StreamReport) => {
        await report.startTimerPromise(
          'Setting up production directory',
          async () => {
            await xfs.mkdirpPromise(outDirectoryPath)
            await copyFile(
              rootDirectoryPath,
              outDirectoryPath,
              Filename.lockfile,
            )
            await copyFile(
              rootDirectoryPath,
              outDirectoryPath,
              configuration.get(`rcFilename`),
            )
            await copyFile(workspace.cwd, outDirectoryPath, Filename.manifest)
            const yarnExcludes: Array<PortablePath> = []
            const checkConfigurationToExclude = <
              K extends KeysMatching<ConfigurationValueMap, PortablePath>,
            >(
              config: K,
            ): void => {
              try {
                if (configuration.get(config)) {
                  yarnExcludes.push(configuration.get(config))
                }
              }
              catch (_) {
                // noop
              }
            }
            checkConfigurationToExclude('installStatePath')
            checkConfigurationToExclude('cacheFolder')
            checkConfigurationToExclude('deferredVersionFolder')

            await configuration.triggerHook(
              (hooks: Hooks) => {
                return hooks.populateYarnPaths
              },
              project,
              (path: PortablePath | null) => {
                if (path) {
                  yarnExcludes.push(path)
                }
              },
            )

            await copyFolder(
              rootDirectoryPath,
              outDirectoryPath,
              `.yarn` as PortablePath,
              yarnExcludes,
            )
          },
        )

        await report.startTimerPromise(
          'Installing production version',
          async () => {
            const outConfiguration = await Configuration.find(
              outDirectoryPath,
              this.context.plugins,
            )

            if (this.stripTypes) {
              const packageExtensions: PackageExtensions = await outConfiguration.getPackageExtensions()

              for (const extensionsByIdent of packageExtensions.values()) {
                for (const [, extensionsByRange] of extensionsByIdent) {
                  for (const extension of extensionsByRange) {
                    if (extension.type === PackageExtensionType.Dependency && extension.descriptor.scope === 'types') {
                      extension.status = PackageExtensionStatus.Inactive
                    }
                  }
                }
              }
            }

            const {
              project: outProject,
              workspace: outWorkspace,
            } = await Project.find(outConfiguration, outDirectoryPath)
            if (!outWorkspace) {
              throw new WorkspaceRequiredError(project.cwd, this.context.cwd)
            }

            if (this.production) {
              outWorkspace.manifest.devDependencies.clear()
            }

            // carry root package resolutions into out project
            for (const [original, resolution] of project.resolutionAliases) {
              outProject.resolutionAliases.set(original, resolution)
            }

            const outCache = await Cache.find(outConfiguration, {
              immutable: false,
              check: false,
            })

            const multiFetcher = outConfiguration.makeFetcher()
            const realResolver = configuration.makeResolver()
            const multiResolver = new MultiResolver([
              new LockfileResolver(realResolver),
              realResolver,
            ])

            const resolver = new ProductionInstallResolver({
              project,
              resolver: multiResolver,
              stripTypes: this.stripTypes,
            })

            const fetcher = new ProductionInstallFetcher({
              cache,
              fetcher: multiFetcher,
              project,
            })

            await this.modifyOriginalResolutions(outProject, resolver, {
              project: outProject,
              fetchOptions: {
                cache: outCache,
                project: outProject,
                fetcher,
                checksums: outProject.storedChecksums,
                report,
              },
              resolver,
              report,
            })

            await outProject.install({
              cache: outCache,
              report,
              immutable: false,
              fetcher,
              resolver,
              checkResolutions: false,
              persistProject: false,
            })

            await report.startTimerPromise(
              'Cleaning up unused dependencies',
              async () => {
                const toRemove: Array<PortablePath> = []

                toRemove.push(
                  ...(await this.getPatchSourcesToRemove(outProject, outCache)),
                )

                for (const locatorPath of toRemove) {
                  if (await xfs.existsPromise(locatorPath)) {
                    report.reportInfo(
                      MessageName.UNUSED_CACHE_ENTRY,
                      `${ppath.basename(
                        locatorPath,
                      )} appears to be unused - removing`,
                    )
                    await xfs.removePromise(locatorPath)
                  }
                }
              },
            )
          },
        )

        if (this.pack) {
          await report.startTimerPromise('Packing workspace ', async () => {
            await packUtils.prepareForPack(workspace, { report }, async () => {
              report.reportJson({ base: workspace.cwd })

              const files = await packUtils.genPackList(workspace)

              for (const file of files) {
                report.reportInfo(null, file)
                report.reportJson({ location: file })
                if (file.endsWith(Filename.manifest)) {
                  const manifest = await packUtils.genPackageManifest(workspace)
                  await xfs.writeJsonPromise(
                    ppath.resolve(outDirectoryPath, file),
                    manifest,
                  )
                }
                else {
                  await copyFile(workspace.cwd, outDirectoryPath, file)
                }
              }
            })
          })

          if (this.injectCjsPnp) {
            // Expermental

            await report.startTimerPromise('Injecting .pnp.cjs call into file ', async () => {
              const fileToInjectPath: PortablePath = npath.isAbsolute(this.injectCjsPnp)
                ? npath.toPortablePath(this.injectCjsPnp)
                : ppath.join(outDirectoryPath, npath.toPortablePath(this.injectCjsPnp))

              await xfs.statPromise(fileToInjectPath)

              const fileToInject = await xfs.readFilePromise(fileToInjectPath, 'utf8')

              const outConfiguration = await Configuration.find(
                outDirectoryPath,
                this.context.plugins,
              )
              const {
                project: outProject,
              } = await Project.find(outConfiguration, outDirectoryPath)

              const {
                cjs: cjsPnpFile,
              } = getPnpPath(outProject)
              const relativeCjsPnpFilePath = ppath.relative(ppath.dirname(fileToInjectPath), cjsPnpFile)

              const injectedFile = `require('${relativeCjsPnpFilePath}').setup()\n${fileToInject}`
              await xfs.writeFilePromise(fileToInjectPath, injectedFile)
            })
          }
        }
      },
    )

    return report.exitCode()
  }

  private async getPatchSourcesToRemove(
    project: Project,
    cache: Cache,
  ): Promise<Array<PortablePath>> {
    const patchedPackages: Array<Package> = []
    project.storedPackages.forEach((storedPackage) => {
      if (storedPackage.reference.startsWith('patch:')) {
        patchedPackages.push(storedPackage)
      }
    })
    const toRemove: Array<PortablePath> = []
    for (const patchedPackage of patchedPackages) {
      const {
        sourceLocator, 
      } = patchUtils.parseLocator(patchedPackage)
      const sourcePackage = project.storedPackages.get(
        sourceLocator.locatorHash,
      )
      if (!sourcePackage) {
        // This should be an error but currently not going to throw
        break
      }
      const dependencies = await dependenciesUtils.getDependents(
        project,
        sourcePackage,
      )
      if (
        dependencies.filter(
          (pkg) => pkg.locatorHash !== patchedPackage.locatorHash,
        ).length > 0
      ) {
        const locatorPath = cache.getLocatorPath(
          sourceLocator,
          project.storedChecksums.get(sourceLocator.locatorHash) ?? null,
        )
        if (locatorPath) {
          toRemove.push(locatorPath)
        }
      }
    }
    return toRemove
  }

  private async modifyOriginalResolutions(
    project: Project,
    resolver: Resolver,
    opts: ResolveOptions,
  ) {
    await opts.report.startTimerPromise(
      'Modifying original install state',
      async () => {
        for (const [
          locatorHash,
          originalPackage,
        ] of project.originalPackages.entries()) {
          const resolvedPackage = await resolver.resolve(originalPackage, opts)
          project.originalPackages.set(locatorHash, resolvedPackage)
        }
      },
    )
  }
}
