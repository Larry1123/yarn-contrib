/*
 * Copyright 2020 Larry1123
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// types

import type {
  Descriptor,
  Locator,
  MinimalResolveOptions,
  Package,
  ResolveOptions,
  Resolver,
  IdentHash,
  Project,
} from '@yarnpkg/core'

// imports

import {
  LinkType,
  WorkspaceResolver,
} from '@yarnpkg/core'

export class ProductionInstallResolver implements Resolver {
  protected readonly resolver: Resolver
  protected readonly project: Project
  protected readonly stripTypes: boolean

  constructor({
    resolver,
    project,
    stripTypes = true,
  }: {
    resolver: Resolver
    project: Project
    stripTypes: boolean
  }) {
    this.resolver = resolver
    this.project = project
    this.stripTypes = stripTypes
  }

  supportsDescriptor(
    descriptor: Descriptor,
    opts: MinimalResolveOptions,
  ): boolean {
    descriptor = opts.project.configuration.normalizeDependency(descriptor)
    return this.resolver.supportsDescriptor(descriptor, {
      project: opts.project,
      resolver: this,
    })
  }

  supportsLocator(locator: Locator, opts: MinimalResolveOptions): boolean {
    locator = opts.project.configuration.normalizeLocator(locator)
    return this.resolver.supportsLocator(locator, {
      project: opts.project,
      resolver: this,
    })
  }

  shouldPersistResolution(
    locator: Locator,
    opts: MinimalResolveOptions,
  ): boolean {
    locator = opts.project.configuration.normalizeLocator(locator)
    if (locator.reference.startsWith(WorkspaceResolver.protocol)) {
      return false
    }
    else {
      return this.resolver.shouldPersistResolution(locator, {
        project: opts.project,
        resolver: this,
      })
    }
  }

  bindDescriptor(
    descriptor: Descriptor,
    fromLocator: Locator,
    opts: MinimalResolveOptions,
  ): Descriptor {
    descriptor = opts.project.configuration.normalizeDependency(descriptor)
    fromLocator = opts.project.configuration.normalizeLocator(fromLocator)
    return this.resolver.bindDescriptor(descriptor, fromLocator, {
      project: opts.project,
      resolver: this,
    })
  }

  getResolutionDependencies(
    descriptor: Descriptor,
    opts: MinimalResolveOptions,
  ): Record<string, Descriptor> {
    descriptor = opts.project.configuration.normalizeDependency(descriptor)
    return this.resolver.getResolutionDependencies(descriptor, {
      project: opts.project,
      resolver: this,
    })
  }

  async getCandidates(
    descriptor: Descriptor,
    dependencies: Record<string, Package>,
    opts: ResolveOptions,
  ): Promise<Array<Locator>> {
    descriptor = opts.project.configuration.normalizeDependency(descriptor)
    if (
      descriptor.range.startsWith(WorkspaceResolver.protocol) &&
      descriptor.range !== `${WorkspaceResolver.protocol}.`
    ) {
      const workplace = this.project.getWorkspaceByDescriptor(descriptor)
      return [workplace.anchoredLocator]
    }
    else {
      return this.resolver.getCandidates(descriptor, dependencies, {
        project: opts.project,
        resolver: this,
        fetchOptions: opts.fetchOptions,
        report: opts.report,
      })
    }
  }

  async resolve(locator: Locator, opts: ResolveOptions): Promise<Package> {
    locator = opts.project.configuration.normalizeLocator(locator)
    const resolve = async (): Promise<Package> => {
      if (
        locator.reference.startsWith(WorkspaceResolver.protocol) &&
        locator.reference !== `${WorkspaceResolver.protocol}.`
      ) {
        const workspace = this.project.getWorkspaceByLocator(locator)
        return {
          ...locator,
          version: workspace.manifest.version || `0.0.0`,
          languageName: `unknown`,
          linkType: LinkType.HARD,
          dependencies: new Map([...workspace.manifest.dependencies]),
          peerDependencies: new Map([...workspace.manifest.peerDependencies]),
          dependenciesMeta: workspace.manifest.dependenciesMeta,
          peerDependenciesMeta: workspace.manifest.peerDependenciesMeta,
          bin: workspace.manifest.bin,
        }
      }
      return this.resolver.resolve(locator, {
        project: opts.project,
        resolver: this,
        fetchOptions: opts.fetchOptions,
        report: opts.report,
      })
    }
    const resolvedPackage = await resolve()

    const dependencies: Map<IdentHash, Descriptor> = new Map<
    IdentHash,
    Descriptor
    >()
    for (const [hash, descriptor] of resolvedPackage.dependencies.entries()) {
      if (this.stripTypes && descriptor.scope === 'types') {
        continue
      }
      dependencies.set(hash, descriptor)
    }
    return {
      ...resolvedPackage,
      dependencies,
    }
  }

  async getSatisfying(descriptor: Descriptor, dependencies: Record<string, Package>, locators: Array<Locator>, opts: ResolveOptions): Promise<{
    locators: Array<Locator>
    sorted: boolean
  }> {
    descriptor = opts.project.configuration.normalizeDependency(descriptor)
    locators = locators.map(locator => opts.project.configuration.normalizeLocator(locator))
    const packageExtensions = await opts.project.configuration.getPackageExtensions()
    Object.keys(dependencies).forEach(key => dependencies[key] = opts.project.configuration.normalizePackage(dependencies[key], {packageExtensions}))
    return this.resolver.getSatisfying(descriptor, dependencies, locators, {
      project: opts.project,
      resolver: this,
      fetchOptions: opts.fetchOptions,
      report: opts.report,
    })
  }
}
